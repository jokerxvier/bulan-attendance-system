<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center">
        <div class="sidebar-brand-icon rotate-n-15">
            <img id="profile-img" class="rounded-circle profile-img-card" src="{{ asset('assets/img/logo.png') }}"  width="40"/>
        </div>
        <div class="sidebar-brand-text mx-3">LGU - Bulan </div>
    </a>

    
        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span></a>
        </li>

    @hasanyrole('super-admin|admin')

        <hr class="sidebar-divider">

        
        <li class="nav-item">
            <a class="nav-link" href="{{ route('department') }}"><i class="fas fa-fw fa-building"></i><span>Departments</span></a>
        </li>

        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">   
            Reports
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('time.entries') }}">
                <i class="fas fa-fw fa-clock"></i>
                <span>Time Logs</span>
            </a>

            <a class="nav-link collapsed" href="{{ route('time.export') }}">
                <i class="fas fa-fw fa-download"></i>
                <span>Export Timesheets</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">   
            Personnel
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('employee') }}">
                <i class="fas fa-fw fa-cog"></i>
                <span>Personnel</span>
            </a>
        </li>
    @endhasanyrole

    @hasrole('super-admin')
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Admins
        </div>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin') }}">
                <i class="fas fa-fw fa-users"></i>
                <span>User</span></a>
        </li>
    @endhasrole

</ul>
<!-- End of Sidebar -->