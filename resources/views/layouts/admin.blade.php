<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
    </head>
    <body id="page-top"> 
        <div id="wrapper">
            @include('includes.sidebar')

            <div id="content-wrapper" class="d-flex flex-column">
                
                <div class="content">
                    @include('includes.header')
                   
                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        @yield('content')

                    </div>
                </div><!--end content-->

                
            </div><!--end content-wrapper-->

            
        </div><!--end wrapper-->

        @livewireScripts

        <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>
        
        @stack('scripts')

        <script type="text/javascript">
            $(function () {
                $('.form_datetime').datetimepicker({
                    //language:  'fr',
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1
                });

                $('.form_date').datetimepicker({
                    pickTime: false,
                    format: 'mm/dd/yyyy',
                    minView: 2,
                });

                $('.form_time').datetimepicker({
                    pickDate: false,
                    minuteStep: 15,
                    pickerPosition: 'bottom-right',
                    format: 'HH:ii p',
                    autoclose: true,
                    showMeridian: true,
                    startView: 1,
                    maxView: 1,
                    pickerPosition: "top-left"
                });
            });
         </script>
    </body>
</html>
