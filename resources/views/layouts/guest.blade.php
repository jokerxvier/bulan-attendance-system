<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
    </head>
    <body class="bg-gradient-primary">
        <div class="container">
            @yield('content')
        </div><!--end container-->

        @livewireScripts
    </body>
</html>
