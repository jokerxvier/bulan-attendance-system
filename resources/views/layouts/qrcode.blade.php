<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        {{-- <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/> --}}
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="attendance-url" content="{{ route('attendance.timein') }}" />
        
        <title>Bulan Daily Attendance</title>
        
        <!-- Custom fonts for this template-->
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">

        <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/qrcode.css') }}" rel="stylesheet" type="text/css">

        <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

        <style>
            .swal-wide {
                padding: 0px !important;
                border-radius: 0px !important;
            }

            .swal-wide .swal2-loading  {}
            .swal-wide .swal2-html-container{
                padding: 0px
            }
            .icon-head {
              background: #1eaaf3;
              padding: 30px 20px;
              text-align: center
            }
        
            .icon-head .c-icon {
              max-width: 100%;
              width: 130px
            }
        
            .modal-content {
              padding: 20px;
              text-align: center;
              border-radius: 0;
            }
        
            .modal-content h2 {
              color: #1eaaf3;
              text-transform: uppercase;
              border: #1eaaf3 solid 1px;
              padding: 5px 8px;
              margin: 0px 10px 10px 10px;
              font-size: 25px;
            }
        
            .modal-content ul {
              margin: auto;
              margin-bottom: 20px;
              padding: 0;
              width: 50%;
        
            }
        
            .modal-content ul li {
              display: inline-block;
              list-style: none;
              width: 48%;
              font-weight: bold;
            }
        
            .modal-content h3 {
              text-transform: uppercase;
              color: #444545;
            }
        
            .modal-content h4 {
              margin-bottom: 20px;
            }
        
            .modal-content .preload {
              padding: 5px 0;
            }
        
            .modal-content .preload img {
              width: 50px;
            }
          </style>

        <style>
            #preview {
                /* margin: auto;
                width: calc(100% - 30px);
                max-width: 150px; */
                width: 100%;
            }

        </style>

        @livewireStyles
    </head>
    <body>
  
          
        <main class="px-3">
            @yield('content')
        </main>

        @livewireScripts
        <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
        <script type="text/javascript">

            $(function () {

                const attendanceUrl = $('meta[name="attendance-url"]').attr('content');
                const loaderAssets = "{{ asset('assets/img/preloader.gif') }}";
                let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 10,  refractoryPeriod: 10000 });
                startCamera(scanner);
                $('.test-qrcode').show();
                $('.scanner-container').hide();
                scanner.addListener('scan', function (content) {
                    const res = content.split(":");
                    const employeeID = res[1];
                    scanner.stop();
                    if(employeeID) {
                        $.ajax({
                            type:'POST',
                            url: attendanceUrl,
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: {user_id: employeeID },
                            success:function(data){
                                // content = `<h2>${data.time_out === null ?  'Welcome!'  : 'Take Care!'  }</h2>
                                //             <h3>${data.name}</h3> 
                                //             <p class="timeContent">
                                //                 <strong>TIME IN </strong>: ${data.time_in} <br /> 
                                //                 <strong>TIME OUT</strong>:  ${data.time_out ? data.time_out : '---' } 
                                //                 <br /> <br /> I will close in <b></b> milliseconds.
                                //             <p>`

                                content = `<div class="modal-inner" style="width:100%">
                                    <div class="icon-head">
                                        <img src="{{ asset('assets/img/checked-icon.png') }}" class="c-icon" />
                                    </div>
                                    <div class="modal-content">
                                        <h3>${data.time_out === null ?  'Welcome!'  : 'Take Care!'  }</h3>
                                        <h2>${data.name}</h2>
                                        <div class="time-record">
                                            <h4>My Logbook Record</h4>
                                            <ul>
                                                <li>
                                                    Time In :
                                                </li>
                                                <li>
                                                    ${data.time_in} 
                                                </li>
                                                <li>
                                                    Time Out : 
                                                </li>
                                                <li>
                                                    ${data.time_out ? data.time_out : '---' } 
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="msg-response">
                                            Closing  in <b> 1290 </b> Milliseconds 
                                        </div>
                                        <div class="preload">
                                            <img src="${loaderAssets}" alt="">
                                        </div>
                                    </div>
                                </div>`


                                showSuccessAlert(scanner, 'Your Timesheet Record Today', content);
                               
                            },
                            error:function(error){

                                startCamera(scanner);

                                if(error.status === 404) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'User not Found!',
                                        timer: 1500,
                                        showConfirmButton: false,
                                        timerProgressBar: true,
                                    })
                                }else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong! Please contact support!',
                                        timer: 1500,
                                        showConfirmButton: false,
                                        timerProgressBar: true,
                                    })
                                }
                            }
                    });
                        
                        $('.scanner-container').hide();
                        $('.test-qrcode').show();
                    }else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops... Something went wrong!',
                            text: 'Cannot Read QR Code',
                            timer: 1500,
                            showConfirmButton: false,
                            timerProgressBar: true,
                        });

                        startCamera(scanner);
                    }
                });

                $('.btn-scan').on('click', function () {
                    startCamera(scanner);
                });

                $('.btn-stop').on('click', function () {
                    scanner.stop();
                    $('.scanner-container').hide();
                    $('.test-qrcode').show();
                });
            })

            function startCamera(scanner) 
            {

                Instascan.Camera.getCameras().then(function (cameras) {
                    if (cameras.length > 0) {
                        var selectedCam = cameras[0];

                        $.each(cameras, (i, c) => {
                            if (c.name.indexOf('back') != -1) {
                                selectedCam = c;
                                return false;
                            }
                        });


                        scanner.start(selectedCam);

                        $('.scanner-container').show();
                        $('.test-qrcode').hide();
                    } else {
                        console.error('No cameras found.');
                    }
                    }).catch(function (e) {
                        console.error(e);

                });
            }

            function showSuccessAlert(scanner, title, content)
            {
                let timerInterval
                    Swal.fire({
                    customClass: 'swal-wide',
                    showSpinner: false,
                    html: content,
                    showConfirmButton: false,
                    timer: 5000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.hideLoading();
                        timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer');
                            startCamera(scanner);
                        }else {
                            startCamera(scanner);
                        }
                    })
            }
          
        </script>
    </body>
</html>
