<div class="employee">
    @include('livewire.employee.update')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        @if(!$selectedDepartmentId)
            @include('livewire.employee.create')
        @endif
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Personnel List</h6>
        </div>
        <div class="card-body">
            
            <div class="row align-items-center mb-4">
                <div class="col-md-4 my-2 my-md-0">
                    <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search by Name" name="search" wire:model.debounce.500ms="search">
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Personnel ID</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Department</th>
                            <th>Mobile Number</th>
                            <th width="200">Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{ $employee->id }}</td>
                                <td>{{ $employee->name }}</td>
                                <td>{{ $employee->position }}</td>
                                <td>{{ $employee->department }}</td>
                                <td>{{ $employee->mobile_number }}</td>
                                <td>
                                    <a href="{{ route('employee.show', $employee->id) }}"  class="btn btn-success btn-sm mb-2">View</a>
                                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $employee->id }})" class="btn btn-primary btn-sm mb-2">Edit</button>
                                    <button wire:click="$emit('triggerDelete', {{ $employee->id  }})" class="btn btn-danger btn-sm mb-2">Delete</button>

                                </td>
                            </tr>
                        @endforeach

                        @if($employees->isEmpty())
                            <tr><td colspan="6">No Available Data</td></tr>
                        @endif
                    </tbody>
                </table>

            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="d-flex align-items-end flex-column">
                        {{ $employees->links() }}
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerDelete', id => {
        
            Swal.fire({
                title: 'Are You Sure?',
                text: 'Record will be deleted!',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                confirmButtonText: 'Delete!'
            }).then((result) => {
       
                if (result.value) {
                    @this.call('delete',id)
                    Swal.fire({title: 'Record has been deleted successfully!', icon: 'success'});
                } else {
                    Swal.fire({
                        title: 'Operation Cancelled!',
                        icon: 'error'
                    });
                }
            });
        });
    })
</script>
@endpush


