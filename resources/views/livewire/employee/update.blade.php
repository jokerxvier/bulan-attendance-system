<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Personnel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click.prevent="cancel()" >
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <input type="text" class="form-control  @error('name') is-invalid @enderror" placeholder="Enter Employee Name" wire:model.defer="name">
                        @error('name') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Username</label>
                        <input type="text" class="form-control  @error('username') is-invalid @enderror" placeholder="Enter Username" wire:model.defer="username">
                        @error('username') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Password</label>
                        <input type="password" class="form-control  @error('password') is-invalid @enderror" placeholder="Enter Password" wire:model.defer="password">
                        @error('password') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Postion</label>
                        <input type="text" class="form-control  @error('position') is-invalid @enderror"  placeholder="Enter Position Name" wire:model.defer="position">
                        @error('position') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Department</label>
                        <select class="form-control @error('department') is-invalid @enderror" aria-label="Default select example" wire:model.defer="department">
                            <option selected>Select Department</option>
                            @foreach($departments as $dept)
                                <option value="{{ $dept->id }}">{{ $dept->name }}</option>
                            @endforeach
                        </select>
                        @error('department') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                       
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Mobile Number</label>
                        <input type="text" class="form-control @error('mobile_number') is-invalid @enderror"  placeholder="ex. 0928111555" wire:model="mobile_number">
                        @error('mobile_number') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                       
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()"  class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    window.livewire.on('employeeUpdate', () => {
        $('#updateModal').modal('hide');
     });
</script>
@endpush