
    <div class="main-body">
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <img src="https://ui-avatars.com/api/?name={{$user->name}}" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4>{{ $user->name }}</h4>
                      <p class="text-secondary mb-1">{{ $user->position ? $user->position : 'N/A' }}</p>
                      {{-- <button class="btn btn-primary">Follow</button>
                      <button class="btn btn-outline-primary">Message</button> --}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3 visible-print align-items-center text-center">
                @if(config('app.env') === 'local') 
                  {!! QrCode::size(150)->margin(5)->generate("employee_id:$user->id") !!}
                @else 
                  

                  <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('https://portal.bulan4706.com/assets/img/logo.png', .17, true)
                    ->margin(5)->size(300)
                    ->errorCorrection('H')
                    ->generate("employee_id:$user->id")) !!}">
                  {{-- <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(5)->size(150)->generate("employee_id:$user->id")) !!}"> --}}
                @endif
                <p>Personnel QR Code</p>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{ ucfirst($user->name) }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Mobile</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{ ucfirst($user->mobile_number) }}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Position</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{ $user->position ? $user->position : 'N/A' }}
                    </div>
                  </div>

                  <hr>

                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Department</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{ $user->department ? $user->department : 'N/A' }}
                    </div>
                  </div>
                </div>
              </div>
              <div class="row gutters-sm">
                <div class="col-sm-12 mb-3">
                  <div class="card h-100">
                    <div class="card-body">
                        <h5 class="d-flex align-items-center mb-3">Time Entries</h5>
                          
                        <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    {{-- <th>ID</th> --}}
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Day</th>
                                    <th>Time In</th>
                                    <th>Time Out</th>
                                    <th></th>
                                    <th>Time In</th>
                                    <th>Time Out</th>
                                    {{-- <th>Status</th> --}}
                                    <th>Total</th>
                                    
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach($time_entries as $time_entry)
                                    <tr>
                                        {{-- <td>{{ $time_entry->id }}</td> --}}
                                        <td>{{ $time_entry->user->name }}</td>
                                        <td>{{ optional($time_entry->created_at)->format('F j, Y') }}</td>
                                        <td>{{ optional($time_entry->created_at)->format('l') }}</td>
                                        <td>{{ optional($time_entry->time_in_am)->format('g:i A') }}</td>
                                        <td>{{ optional($time_entry->time_out_am)->format('g:i A') }}</td>
                                        <td>&nbsp;</td>
                                        <td>{{ optional($time_entry->time_in_pm)->format('g:i A') }}</td>
                                        <td>{{ optional($time_entry->time_out_pm)->format('g:i A') }}</td>
                                        {{-- <td>
                                            @if(optional($time_entry)->time_in_status)
                                                <span class="badge badge-pill badge-success">On Time</span>
                                            @else
                                            <span class="badge badge-pill badge-danger">Late</span>
                                            @endif                                
                                        </td> --}}
                                        <td>{{ optional($time_entry)->total_time }}</td>
                                        
                                    </tr>
                                @endforeach

                                @if($time_entries->isEmpty())
                                  <tr><td colspan="11">No Available Data</td></tr>
                                @endif

                            </tbody>
                          </table>    

                          
                        </div>         
                        
                        <div class="row">
                          <div class="col-sm-12 col-md-12">
                              <div class="d-flex align-items-end flex-column">
                                  {{ $time_entries->links() }}
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="d-flex align-items-end flex-column">
                            {{ $time_entries->links() }}
                        </div>
                    </div>
                   
                    </div>
                </div>
               
              </div>
            </div>
          </div>
        </div>
    </div>