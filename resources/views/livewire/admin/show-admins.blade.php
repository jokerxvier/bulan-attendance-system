<div class="employee">
    @include('livewire.admin.update')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        @include('livewire.admin.create')
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Admin List</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($admins as $admin)
                            <tr>
                                <td>{{ $admin->id }}</td>
                                <td>{{ $admin->name }}</td>
                                <td>{{ $admin->username }}</td>
                                <td>{{ $admin->getRoleNames()->first() }}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $admin->id }})" class="btn btn-primary btn-sm">Edit</button>
                                    <button wire:click="$emit('triggerDelete', {{ $admin->id  }})" class="btn btn-danger btn-sm">Delete</button> 
                                    
                                </td>
                            </tr>
                        @endforeach

                        @if($admins->isEmpty())
                            <tr><td colspan="6">No Available Data</td></tr>
                        @endif
                    </tbody>
                </table>

            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="d-flex align-items-end flex-column">
                        {{ $admins->links() }}
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerDelete', id => {
        
            Swal.fire({
                title: 'Are You Sure?',
                text: 'Record will be deleted!',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                confirmButtonText: 'Delete!'
            }).then((result) => {
       
                if (result.value) {
                    @this.call('delete',id)
                    Swal.fire({title: 'Record has been deleted successfully!', icon: 'success'});
                } else {
                    Swal.fire({
                        title: 'Operation Cancelled!',
                        icon: 'error'
                    });
                }
            });
        });
    })

    window.livewire.on('adminDeleteWarning', () => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Cannot Delete Current Login User!',
        })
    });
</script>
@endpush


