<button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal" wire:click.prevent="cancel()">
    <i class="fas fa-plus fa-sm text-white-50"></i>  Create Admin
</button>

<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <input type="text" class="form-control  @error('name') is-invalid @enderror" placeholder="Enter Employee Name" wire:model.defer="name">
                        @error('name') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Username</label>
                        <input type="text" class="form-control  @error('username') is-invalid @enderror" placeholder="Enter Username" wire:model.defer="username">
                        @error('username') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Password</label>
                        <input type="password" class="form-control  @error('password') is-invalid @enderror" placeholder="Enter Password" wire:model.defer="password">
                        @error('password') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Role</label>
                        <select class="form-control @error('role') is-invalid @enderror" aria-label="Default select example" wire:model.defer="role">
                            <option selected>Select Role</option>
                            <option value="admin">Admin</option>
                            <option value="super-admin">Super Admin</option>
                            
                        </select>
                        @error('role') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                       
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    window.livewire.on('employeeStore', () => {
        $('#exampleModal').modal('hide');
     });
</script>
@endpush