<div class="time-entry col-md-6">
   
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Export Timesheet</h6>
        </div>
        <div class="card-body">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif
            <div class="table-responsive">
                <form>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Department (optional)</label>
                        <select class="form-control  @error('user_id') is-invalid @enderror" aria-label="Default select example" wire:model="department">
                            <option selected value="0">Select Department</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                        </select>
                        @error('department') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>


                    <div class="form-group">
                        <label for="exampleFormControlInput1">Personnel (optional)</label>
                        <select class="form-control  @error('user_id') is-invalid @enderror" wire:model.defer="user_id">
                            <option  value="0" selected>Select Personnel</option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('user_id') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>

                    

                    <div class="form-group">
                        <label for="exampleFormControlInput1">From Date</label>

                        <div class="input-group date  mb-3 form_date">
                            <input class="form-control @error('fromDate') is-invalid @enderror from-date" type="text">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                            </div>
                            @error('fromDate') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">To Date</label>

                        <div class="input-group date  mb-3 form_date">
                            <input class="form-control @error('toDate') is-invalid @enderror to-date" type="text">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                            </div>
                            @error('toDate') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" wire:click.prevent="export()" class="btn btn-primary close-modal">Export</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $('.from-date').on('change.datetimepicker', function (e) {
        @this.set('fromDate', e.target.value);
    });

    $('.to-date').on('change.datetimepicker', function (e) {
        @this.set('toDate', e.target.value);
    });
</script>
@endpush



