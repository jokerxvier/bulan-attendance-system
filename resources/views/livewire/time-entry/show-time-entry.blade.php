<div class="time-entry">
    @include('livewire.time-entry.update')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        @include('livewire.time-entry.create')
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Time Logs</h6>
        </div>
        <div class="card-body">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif

            <div class="row align-items-center mb-4">
                <div class="col-md-4 my-2 my-md-0">
                    <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search by Name" name="search" wire:model.debounce.500ms="search">
                    </div>
                </div>
            </div>

            
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>ID</th> --}}
                            <th>Name</th>
                            <th>Date</th>
                            <th>Day</th>
                            <th>Time In</th>
                            <th>Time Out</th>
                            <th></th>
                            <th>Time In</th>
                            <th>Time Out</th>
                            {{-- <th>Status</th> --}}
                            <th>Total</th>
                            <th width="200">Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($time_entries as $time_entry)
                            <tr>
                                {{-- <td>{{ $time_entry->id }}</td> --}}
                                <td>{{ $time_entry->user->name }}</td>
                                <td>{{ optional($time_entry->created_at)->format('F j, Y') }}</td>
                                <td>{{ optional($time_entry->created_at)->format('l') }}</td>
                                <td>{{ optional($time_entry->time_in_am)->format('g:i A') }}</td>
                                <td>{{ optional($time_entry->time_out_am)->format('g:i A') }}</td>
                                <td>&nbsp;</td>
                                <td>{{ optional($time_entry->time_in_pm)->format('g:i A') }}</td>
                                <td>{{ optional($time_entry->time_out_pm)->format('g:i A') }}</td>
                                {{-- <td>
                                    @if(optional($time_entry)->time_in_status)
                                        <span class="badge badge-pill badge-success">On Time</span>
                                    @else
                                    <span class="badge badge-pill badge-danger">Late</span>
                                    @endif                                
                                </td> --}}
                                <td>{{ optional($time_entry)->total_time }}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $time_entry->id }})" class="btn btn-primary btn-sm mb-2">Edit</button>
                                    <button wire:click="$emit('triggerDelete', {{ $time_entry->id  }})"  class="btn btn-danger btn-sm mb-2">Delete</button>
                                
                                </td>
                            </tr>
                        @endforeach

                        @if($time_entries->isEmpty())
                            <tr><td colspan="11">No Available Data</td></tr>
                        @endif
                    </tbody>
                </table>               
            </div>

            

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="d-flex align-items-end flex-column">
                        {{ $time_entries->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerDelete', id => {
        
            Swal.fire({
                title: 'Are You Sure?',
                text: 'Record will be deleted!',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                confirmButtonText: 'Delete!'
            }).then((result) => {
       
                if (result.value) {
                    @this.call('delete',id)
                    Swal.fire({title: 'Record has been deleted successfully!', icon: 'success'});
                } else {
                    Swal.fire({
                        title: 'Operation Cancelled!',
                        icon: 'error'
                    });
                }
            });
        });
    })
</script>
@endpush



