<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Time Entry</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <select class="form-control  @error('user_id') is-invalid @enderror" aria-label="Default select example" wire:model.defer="user_id">
                            <option selected>Select User</option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('user_id') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                    </div>


                    <div class="form-group">
                        <label for="exampleFormControlInput1">Date</label>

                        <div class="input-group date  mb-3" id="form_date">
                            <input class="form-control @error('date') is-invalid @enderror" type="text"  id="DateEntry" wire:model.defer="date">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                            </div>
                            @error('date') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Morning</label>

                        <div class="form-row">
                            
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Time In</label>
                                    <div class="input-group date form_time mb-3">
                                        <input class="form-control @error('time_start_am') is-invalid @enderror" type="text"  wire:model.defer="time_start_am" id="dateTimeInAm">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                                        </div>
                                        @error('time_start_am') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Time Out</label>
                                    <div class="input-group date form_time mb-3">
                                        <input class="form-control @error('time_end_am') is-invalid @enderror" type="text"   wire:model.defer="time_end_am" id="dateTimeOutAm">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                                        </div>
            
                                        @error('time_end_am') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Afternoon</label>

                        <div class="form-row">
                            
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Time In</label>
                                    <div class="input-group date form_time mb-3">
                                        <input class="form-control @error('time_start_pm') is-invalid @enderror" type="text"  wire:model.defer="time_start_pm" id="dateTimeInPm">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                                        </div>
                                        @error('time_start_pm') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Time Out</label>
                                    <div class="input-group date form_time mb-3">
                                        <input class="form-control @error('time_end_pm') is-invalid @enderror" type="text"   wire:model.defer="time_end_pm" id="dateTimeOutPm">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></button>
                                        </div>
            
                                        @error('time_end_pm') <span class="invalid-feedback text-left">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                   
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    window.livewire.on('timeEntryUpdate', () => {
        $('#updateModal').modal('hide');
     });
</script>
@endpush

@push('scripts')
<script type="text/javascript">

    window.livewire.on('timeEntryUpdate', () => {
        $('#updateModal').modal('hide');
     });

    $('#DateEntry').on('change.datetimepicker', function (e) {
        @this.set('date', e.target.value);
    });

    $('#dateTimeInAm').on('change.datetimepicker', function (e) {
        @this.set('time_start_am', e.target.value);
    });

    $('#dateTimeOutAm').on('change.datetimepicker', function (e) {
        @this.set('time_end_am', e.target.value);
    });

    $('#dateTimeInPm').on('change.datetimepicker', function (e) {
        @this.set('time_start_pm', e.target.value);
    });

    $('#dateTimeOutPm').on('change.datetimepicker', function (e) {
        @this.set('time_end_pm', e.target.value);
    });

</script>
@endpush