<h2><span  wire:poll>{{ now()->format('g:iA') }}</span></h2>
<div class="date-info">
    <div class="date">
    <ul>
        <li>
        <i class="far fa-calendar-alt"></i> {{ now()->format('m/d/Y') }} </li>
        <li> 
        <i class="fas fa-cloud-sun"></i> {{ now()->format('l') }} </li>
    </ul>
    </div>
</div>
