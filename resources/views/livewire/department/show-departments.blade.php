<div class="department">
    @include('livewire.department.update')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        @include('livewire.department.create')
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Departments</h6>
        </div>

        <div class="card-body">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif

            <div class="row align-items-center mb-4">
                <div class="col-md-4 my-2 my-md-0">
                    <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search by Name" name="search" wire:model.debounce.500ms="search">
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Personnel Count</th>
                            <th>Created At</th>
                            <th width="200">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($departments as $department)
                            <tr>
                                <td>{{ ucfirst($department->name) }}</td>
                                <td>{{  $department->user()->count() }}</td>
                                <td>{{  $department->created_at->format('Y-m-d H:i') }}</td>
                                <td>
                                    <a href="{{ route('department.show', $department->id) }}" class="btn btn-success btn-sm mb-2">View</a>
                                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $department->id }})" class="btn btn-primary btn-sm mb-2">Edit</button>
                                    <button class="btn btn-danger btn-sm mb-2" wire:click="$emit('triggerDelete', {{ $department->id  }})">Delete</button>
                                </td>
                                
                            </tr>
                        @endforeach

                        @if($departments->isEmpty())
                            <tr><td colspan="4">No Available Data</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        @this.on('triggerDelete', departmentId => {
        
            Swal.fire({
                title: 'Are You Sure?',
                text: 'Record will be deleted!',
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#aaa',
                confirmButtonText: 'Delete!'
            }).then((result) => {
       
                if (result.value) {
                    @this.call('delete',departmentId)
                    Swal.fire({title: 'Record has been deleted successfully!', icon: 'success'});
                } else {
                    Swal.fire({
                        title: 'Operation Cancelled!',
                        icon: 'error'
                    });
                }
            });
        });
    })
</script>
@endpush

