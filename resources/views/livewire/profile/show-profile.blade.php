<div class="main-body">
    @include('livewire.profile.update')
    <div class="row gutters-sm">
        <div class="col-md-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-column align-items-center text-center">
                        <img src="https://ui-avatars.com/api/?name={{$user->name}}" class="rounded-circle" width="150">
                        <div class="mt-3">
                        <h4>{{ $user->name }}</h4>
                        <p class="text-secondary mb-1">{{ $user->position ? $user->position : 'N/A' }}</p>
                        {{-- <button class="btn btn-primary">Follow</button>
                        <button class="btn btn-outline-primary">Message</button> --}}
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="card mt-3 visible-print align-items-center text-center">
                @if(config('app.env') === 'local') 
                {!! QrCode::size(150)->margin(5)->generate("employee_id:$user->id") !!}
                @else 
                

                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('https://portal.bulannow.com/assets/img/logo.png', .17, true)
                    ->margin(5)->size(300)
                    ->errorCorrection('H')
                    ->generate("employee_id:$user->id")) !!}">
                
                @endif
                <p>Personnel QR Code</p>
            </div> --}}
        </div>
        <div class="col-md-8">
            <div class="card mb-3">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">User Information</h6>
                    <div class="dropdown no-arrow">
                        <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $user->id }})" class="btn btn-primary btn-sm">Edit</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                        <h6 class="mb-0">Full Name</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        {{ ucfirst($user->name) }}
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <h6 class="mb-0">Username</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                        {{ $user->username }}
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <h6 class="mb-0">Mobile</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ ucfirst($user->mobile_number) }}
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <h6 class="mb-0">Position</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $user->position ? $user->position : 'N/A' }}
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-3">
                        <h6 class="mb-0">Department</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $user->department ? $user->department : 'N/A' }}
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</div>

