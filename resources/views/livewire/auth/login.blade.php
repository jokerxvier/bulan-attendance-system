<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">

            <div class="col-lg-12">
                
                
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Login</h1>
                    </div>

                    @if(session('message'))
                        <div class="alert alert-info" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    <form class="user" wire:submit.prevent="login">
                        <div class="form-group">
                            <input type="text" class="form-control form-control-user @error('username') is-invalid @enderror" placeholder="Enter Username..." wire:model.defer="username">

                            @error('username') <span class="invalid-feedback text-left">{{ $message }}</span> @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="exampleInputPassword" placeholder="Password" wire:model.defer="password">
                            @error('password') <span class="invalid-feedback text-left">{{ $message }}</span> @enderror
                        </div>
                        
                        <button  type="submit" class="btn btn-primary btn-user btn-block">
                            Login
                        </button>
                    </form>
                   
                    {{-- <div class="text-center">
                        <a class="small" href="forgot-password.html">Forgot Password?</a>
                    </div>
                    <div class="text-center">
                        <a class="small" href="register.html">Create an Account!</a>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>