@extends('layouts.guest')
@section('content')
      <!-- Outer Row -->
      <div class="row justify-content-center my-5">
        <div class="col-12">
            <div class="text-center">
                <img id="profile-img" class="rounded-circle profile-img-card" src="{{ asset('assets/img/logo.png') }}"  width="150"/>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-9">
            <livewire:auth.login />
        </div>

    </div>
@stop