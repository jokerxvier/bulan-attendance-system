@extends('layouts.qrcode')
@section('content')
<div class="container">
    <div class="logbox-main center">
      <div class="header-logo">
        <a href="#">
          <img src="{{ asset('assets/img/logo-qr.png')}}" alt="">
        </a>

      </div>
      <div class="content">
        <div class="lgu-logo">
          <img src="{{ asset('assets/img/lgu-bulan.png') }}" alt="" width="80">
          <p>Municipality of Bulan
            <br/> LOCAL GOVERMENT UNIT </p>
        </div>
        <div class="daily-log">
          <span class="logbook-span">Daily Logbook</span>
          <div class="time">
            <livewire:show-timer />

            <div class="qr-preview">
                <div class="test-qrcode">
                    <img src="{{ asset('assets/img/qr-img.png') }}" alt="">
                </div>
                <div class=" scanner-container">
                    <div class="vid-container">
                        <video id="preview"></video>
                    </div>
                    
                </div>
            </div>
            
          </div>
         
        </div>
        <div class="optional-btn">
          <a href="#" class="btn-scan btn-scan"><i class="fas fa-camera"></i> Scan QR Code</a>
          {{-- <a class=" btn-scan btn-stop"><i class="fas fa-cancel"></i> Scan Stop</a> --}}
        </div>

      </div>
    </div>
  </div>

@stop