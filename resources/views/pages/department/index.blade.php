@extends('layouts.admin')
@section('content')

    <div class="row  justify-content-center my-5">
        <div class="col-md-11">
           
            <livewire:department.show-departments />
        </div>
    </div> 
@stop