@extends('layouts.admin')
@section('content')
    <livewire:employee.show-employee :selectedDepartmentId="$department->id"/>
@stop