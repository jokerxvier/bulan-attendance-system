@extends('layouts.admin')
@section('content')

    @hasanyrole('super-admin|admin')
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>

        <!-- Content Row -->
        <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Personnel</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $total_employee }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-briefcase fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end card-->

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Total Departments</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $total_department }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-building fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end card-->

            <!-- Pending Requests Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">New Personnel</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $total_new }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-user-clock fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                
            </div>
        </div><!--end row-->

        <div class="row">

            <div class="col-lg-6 mb-4">

                <!-- Illustrations -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Admin Logs</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Time</th>
                                        <th>Description</th>
                                        <th>Done By</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                    @foreach($latestActivities as $activity)
                                        <tr>
                                            <td>{{ $activity->created_at->diffForHumans() }}</td>
                                            <td>{{ $activity->description }}</td>
                                            <td>{{ $activity->causer->name }}</td>
                                        
                                        </tr>
                                    @endforeach

                                    @if($latestActivities->isEmpty())
                                        <tr><td colspan="3">No Available Data</td></tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                
                    </div>
                </div>

            </div><!--end col-lg-6-->

            <div class="col-xl-6 col-lg-6">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div
                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Department Overview</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        {!! $chart->container() !!}
                    </div>
                </div>

            </div>

            
        
        </div><!--end row-->
    @else
        @php $user = auth()->user(); @endphp
        <livewire:employee.show :user="$user"/>
    @endhasanyrole

     
@stop

@hasanyrole('super-admin|admin')

    @push('scripts')
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>
            
        {!! $chart->script() !!}
    @endpush
@endhasanyrole