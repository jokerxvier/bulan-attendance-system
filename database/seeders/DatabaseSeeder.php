<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            RoleSeeder::class
        ]);

        $super_admin = User::whereUsername('super-admin')->first();

        if(!$super_admin) {
            \App\Models\User::factory(1)->create(['username' => 'super-admin', 'name' => 'Super Admin', 'password' => bcrypt('password')]);
        }
            
        $admin = User::whereUsername('admin')->first();
        if (!$admin) {
            \App\Models\User::factory(1)->create(['username' => 'admin', 'name' => 'Admin', 'password' => bcrypt('password')]);
        }

       
    }
}
