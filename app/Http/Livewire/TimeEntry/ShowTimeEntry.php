<?php
namespace App\Http\Livewire\TimeEntry;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\TimeEntry;
use App\Models\User;
use Carbon\Carbon;

class ShowTimeEntry extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $date = '';
    public $timeEntry_id;
    public $user_id;
    public $time_start_am;
    public $time_end_am;
    public $time_start_pm;
    public $time_end_pm;
    public $updateMode = false;
    public $search = '';

    protected $rules = [
        'user_id' => 'required|integer',
        'date' => 'required|date_format:m/d/Y',
        'time_start_am' => 'required|date_format:h:i a',
        'time_end_am' => 'required|date_format:h:i a',
        'time_start_pm' => 'required|date_format:h:i a',
        'time_end_pm' => 'required|date_format:h:i a',
    ];

    private function resetInputFields(){
        $this->user_id = '';
        $this->time_start_am = '';
        $this->time_end_am = '';
        $this->time_start_pm = '';
        $this->time_end_pm = '';
        $this->date = '';
        $this->timeEntry_id = null;
    }

    public function store() {
        $validatedData = $this->validate();

        $user = User::find($this->user_id);
        $formatDate = Carbon::parse($this->date)->format('Y-m-d');

        $startTimeAm = Carbon::parse($formatDate . ' ' . $this->time_start_am)->format('Y-m-d H:i');
        $endTimeAm = Carbon::parse($formatDate . ' ' . $this->time_end_am)->format('Y-m-d H:i');
        $startTimePm = Carbon::parse($formatDate . ' ' . $this->time_start_pm)->format('Y-m-d H:i');
        $endTimePm = Carbon::parse($formatDate . ' ' . $this->time_end_pm)->format('Y-m-d H:i');

        $timeEntry = TimeEntry::whereDate('created_at', $formatDate)->whereUserId($user->id)->first();

        if ($timeEntry) {
            $timeEntry->update([
                'time_start_am' => $startTimeAm,
                'time_end_am' => $endTimeAm,
                'time_start_pm' => $startTimePm,
                'time_end_pm' => $endTimePm,
                'user_id' => $this->user_id,
                'created_at' => Carbon::parse($formatDate)->format('Y-m-d H:i')
            ]);
        }else {
            $timeEntry = TimeEntry::create([
                'time_start_am' => $startTimeAm,
                'time_end_am' => $endTimeAm ,
                'time_start_pm' => $startTimePm,
                'time_end_pm' => $endTimePm,
                'user_id' => $this->user_id,
                'created_at' => Carbon::parse($formatDate)->format('Y-m-d H:i')
            ]);
        }
 
        session()->flash('message', 'Time Entry Created Successfully.');

        activity()->withProperties(['created' => $timeEntry])->log("TimeEntry Created: $user->name");

        $this->resetInputFields();

        $this->emit('timeEntryStore'); // Close model to using to jquery
    }

    public function edit($id)
    {
        $this->updateMode = true;
        $timeEntry = TimeEntry::findOrFail($id);
        $this->timeEntry_id = $timeEntry->id;

        $user = User::where('id', $timeEntry->user_id)->first();

        $this->user_id = $user->id;
        $this->date = $timeEntry->created_at->format('m/d/Y');
        $this->time_start_am = $timeEntry->time_start_am ? $timeEntry->time_start_am->format('h:i a') : '';
        $this->time_end_am = $timeEntry->time_end_am ? $timeEntry->time_end_am->format('h:i a') : '';

        $this->time_start_pm = $timeEntry->time_start_pm ? $timeEntry->time_start_pm->format('h:i a') : '';
        $this->time_end_pm =  $timeEntry->time_end_pm ? $timeEntry->time_end_pm->format('h:i a'): '';
    }

    public function update()
    {
        $validatedData = $this->validate();

        if ($this->timeEntry_id) {
            $timeEntry = TimeEntry::findOrFail($this->timeEntry_id);

            $user = User::find($this->user_id);
            $formatDate = Carbon::parse($this->date)->format('Y-m-d');
    
            $startTimeAm = Carbon::parse($formatDate . ' ' . $this->time_start_am)->format('Y-m-d H:i');
            $endTimeAm = Carbon::parse($formatDate . ' ' . $this->time_end_am)->format('Y-m-d H:i');
            $startTimePm = Carbon::parse($formatDate . ' ' . $this->time_start_pm)->format('Y-m-d H:i');
            $endTimePm = Carbon::parse($formatDate . ' ' . $this->time_end_pm)->format('Y-m-d H:i');

            $timeEntry->update([
                'time_start_am' => $startTimeAm,
                'time_end_am' => $endTimeAm ,
                'time_start_pm' => $startTimePm,
                'time_end_pm' => $endTimePm,
                'user_id' => $this->user_id,
                'created_at' => Carbon::parse($formatDate)->format('Y-m-d H:i')
            ]);

            session()->flash('message', 'TimeEntry Updated Successfully.');
            $this->resetInputFields();
            $this->emit('timeEntryUpdate');
        }
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function delete($id) {
        if($id){
            $timeEntry = TimeEntry::findOrFail($id);
            $user = User::find($timeEntry->user_id);
            activity()->withProperties(['deleted' => $timeEntry])->log("TimeEntry Deleted: $user->name");
            $timeEntry->delete();

            session()->flash('message', 'Time Entry Deleted Successfully.');
        }
    }

    public function render()
    {
        $time_entries = TimeEntry::whereHas('user', function ($query) {
            $query->where('name', 'like', '%'.$this->search.'%');
        })->orderBy('created_at','desc')->simplePaginate(20);
        $users = User::role('employee')->get();

        
        return view('livewire.time-entry.show-time-entry', compact('time_entries', 'users'));
    }
}
