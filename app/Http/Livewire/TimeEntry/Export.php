<?php

namespace App\Http\Livewire\TimeEntry;

use Livewire\Component;
use App\Exports\TimesheetExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;
use App\Models\Department;
use Carbon\Carbon;

class Export extends Component
{
    public $fromDate;
    public $toDate;
    public $user_id = 0;
    public $department;
    public $users = [];

    protected $rules = [
        'user_id' => 'nullable',
        'department' => 'nullable',
        'fromDate' => 'required|date_format:m/d/Y',
        'toDate' => 'required|date_format:m/d/Y|after_or_equal:fromDate',
    ];

    public function export() 
    {
        $validatedData = $this->validate();

        $from = Carbon::parse($this->fromDate)->format('Y-m-d');
        $to = Carbon::parse($this->toDate)->format('Y-m-d');
        $department = $this->department ? $this->department : false;
        $user_id = $this->user_id ? $this->user_id : false;

        if($user_id) {
            return Excel::download(new TimesheetExport($from, $to, false, $user_id), 'reports.xlsx');
        }
        
        if($department) {
            return Excel::download(new TimesheetExport($from, $to, $department, false), 'reports.xlsx');
        }
            
        return Excel::download(new TimesheetExport($from, $to), 'reports.xlsx');
    }

    public function render()
    {
        
        if(!empty($this->department)) {
            $this->users = User::whereDepartmentId($this->department)->orderBy('name', 'ASC')->get();
        }else{
            $this->users = User::role('employee')->orderBy('name', 'ASC')->get();
        }

        $departments = Department::all();

        return view('livewire.time-entry.export', compact('departments'));
    }
}
