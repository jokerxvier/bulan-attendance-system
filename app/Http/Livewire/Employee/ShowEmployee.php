<?php

namespace App\Http\Livewire\Employee;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Department;
use App\Models\User;

class ShowEmployee extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $selectedDepartmentId = null;

    public $department;
    public $name;
    public $username;
    public $password;
    public $position = null;
    public $mobile_number;
    public $departments;
    public $updateMode = false;
    public $user_id;
    public $department_id;
    public $search = '';

    protected $rules = [
        'name' => 'required|string|max:200',
        'username' => 'required|string|alpha_dash|unique:users',
        'password' => 'required|string|min:8',
        'position' => 'nullable|string|max:200',
        'department' => 'required|exists:departments,id',
        'mobile_number' => 'required|regex:/(^(09)\d{9}$)+/|unique:users'
    ];

    private function resetInputFields(){
        $this->name = '';
        $this->username = '';
        $this->password = '';
        $this->position = '';
        $this->department = '';
        $this->mobile_number = '';
    }

    public function mount()
    {
        $this->departments = Department::all();
    }

    public function store()
    {
        $validatedData = $this->validate();

        $user = User::create([
            'name' => $this->name,
            'position' => $this->position,
            'mobile_number' => $this->mobile_number,
            'username' => $this->username,
            'password' => bcrypt($this->password),
            'department_id' => $this->department
        ]);

        $user->assignRole('employee');

        // QrCode::size(500)->margin(5)->generate("employee_id:$user->id", public_path("qrcodes/$user->id" . ".svg"));

        session()->flash('message', 'Employee Created Successfully.');

        activity()->log("Employee Created: $user->name");

        $this->resetInputFields();

        $this->emit('employeeStore'); // Close model to using to jquery
    }

    public function update()
    {

        $this->rules['username'] = 'required|string|alpha_dash|unique:users,username,' .$this->user_id;
        $this->rules['mobile_number'] = 'required|regex:/(^(09)\d{9}$)+/|unique:users,mobile_number,' .$this->user_id;
        $this->rules['password'] = 'nullable|string|min:8';
       
        $validatedData = $this->validate();

        if ($this->user_id) {
            $user = User::find($this->user_id);
            $old = User::find($this->user_id);

            $array = [
                'name' => $this->name,
                'position' => $this->position,
                'mobile_number' => $this->mobile_number,
                'username' => $this->username,
                'password' => bcrypt($this->password),
                'department_id' => $this->department
            ];

            if (!$this->password) {
                $array = Arr::except($array, ['password']);
            }

            $user->update($array);
            $this->updateMode = false;
            activity()->withProperties(['old' => $old, 'new' => $user->refresh()])->log("Employee  Updated: $user->name");
            session()->flash('message', 'Employee Updated Successfully.');
            $this->resetInputFields();
            $this->emit('employeeUpdate'); // Close model to using to jquery

        }
    }

    public function edit($id)
    {
        $this->updateMode = true;
        $user = User::where('id', $id)->first();
        $this->user_id = $id;

        $this->name = $user->name;
        $this->position = $user->position;
        $this->mobile_number = $user->mobile_number;
        $this->username = $user->username;
        $this->password = '';
        $this->department = $user->department_id;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function delete($id)
    {
        if($id){
            $user = User::findOrFail($id);
            activity()->withProperties(['deleted' => $user])->log("Employee  Deleted: $user->name");
            $user->delete();
            session()->flash('message', 'Deparment Deleted Successfully.');
        }
    }

    public function render()
    {   
        if ($this->selectedDepartmentId) {
            $users = User::role('employee')->where('name', 'like', '%'.$this->search.'%')->whereDepartmentId($this->selectedDepartmentId)->simplePaginate(20);
        }else {
            $users = User::role('employee')->where('name', 'like', '%'.$this->search.'%')->simplePaginate(20);
        }
        return view('livewire.employee.show-employee', ['employees' => $users]);
    }
}
