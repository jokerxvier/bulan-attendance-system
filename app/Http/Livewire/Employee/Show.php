<?php

namespace App\Http\Livewire\Employee;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\User;
use App\Models\TimeEntry;

class Show extends Component
{
    use WithPagination;

    public $user; 

    public function render()
    {
        $time_entries = TimeEntry::whereUserId($this->user->id)->orderBy('created_at','desc')->simplePaginate(20);
        return view('livewire.employee.show', compact('time_entries'));
    }
}
