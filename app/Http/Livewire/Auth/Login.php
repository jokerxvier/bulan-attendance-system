<?php

namespace App\Http\Livewire\Auth;
use Illuminate\Validation\ValidationException;
use Livewire\Component;
use Auth;

class Login extends Component
{
    public $username;
    public $password;

    protected $rules = [
        'username' => 'required|min:2',
        'password' => 'required',
    ];

    public function login()
    {
        $validatedData = $this->validate();

        if(\Auth::attempt(array('username' => $this->username, 'password' => $this->password))){
            return redirect()->intended('/');
        }

        throw ValidationException::withMessages(['username' => 'Invalid credentials']);
        
    }

    public function render()
    {
        return view('livewire.auth.login');
    }
}
