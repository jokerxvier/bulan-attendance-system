<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShowTimer extends Component
{
    public function render()
    {
        return view('livewire.show-timer');
    }
}
