<?php

namespace App\Http\Livewire\Department;

use Livewire\Component;
use App\Models\Department;

class ShowDepartments extends Component
{

    public $name;
    public $user_id;
    public $departments;
    public $updateMode = false;
    public $search = '';

    protected $rules = [
        'name' => 'required|max:200',
    ];

    private function resetInputFields(){
        $this->name = '';
    }


    public function store() 
    {
        $validatedData = $this->validate();

        $department = Department::create($validatedData);

        activity()->log("Department Created: $department->name");

        session()->flash('message', 'Deparment Created Successfully.');

        $this->resetInputFields();

        $this->emit('deparmentStore'); // Close model to using to jquery

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $department = Department::where('id',$id)->first();
        $this->user_id = $id;
        $this->name = $department->name;
    }

    public function update()
    {
        $validatedData = $this->validate();

        if ($this->user_id) {

            $old = Department::find($this->user_id);

            $department = Department::find($this->user_id);

            $department->update([
                'name' => $this->name
            ]);

            $this->updateMode = false;

            activity()->withProperties(['old' => $old, 'new' => $department->refresh()])->log("Department Updated: $department->name");
            
            session()->flash('message', 'Department Updated Successfully.');
            $this->resetInputFields();

            $this->emit('deparmentUpdate'); // Close model to using to jquery

        }
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function delete($id)
    {
        if($id){
            $department = Department::findOrFail($id);
            $department->user()->update(['department_id' => null]);
            activity()->withProperties(['deleted' => $department])->log("Department Deleted: $department->name");
            $department->delete();
            session()->flash('message', 'Deparment Deleted Successfully.');
        }
    }


    public function render()
    {
        $this->departments = Department::where('name', 'like', '%'.$this->search.'%')->get();
        return view('livewire.department.show-departments');
    }
}
