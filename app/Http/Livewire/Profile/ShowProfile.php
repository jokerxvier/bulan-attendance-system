<?php

namespace App\Http\Livewire\Profile;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Arr;

class ShowProfile extends Component
{
    public $user_id;
    public $name;
    public $username;
    public $password;
    public $mobile_number;

    protected $rules = [
        'name' => 'required|string|max:200',
        'username' => 'required|string|alpha_dash|unique:users',
        'password' => 'required|string|min:8',
        'mobile_number' => 'nullable|regex:/(^(09)\d{9}$)+/|unique:users'
    ];

    private function resetInputFields(){
        $this->name = '';
        $this->username = '';
        $this->password = '';
        $this->mobile_number = '';
    }

    public function edit($id)
    {

        $user = User::where('id', $id)->first();
        $this->user_id = auth()->user()->id;
        $this->name = $user->name;
        $this->position = $user->position;
        $this->mobile_number = $user->mobile_number;
        $this->username = $user->username;
        $this->password = '';
    }

    public function update()
    {
        $this->rules['username'] = 'required|string|alpha_dash|unique:users,username,' .$this->user_id;
        $this->rules['mobile_number'] = 'nullable|regex:/(^(09)\d{9}$)+/|unique:users,mobile_number,' .$this->user_id;
        $this->rules['password'] = 'nullable|string|min:8';
        

        $validatedData = $this->validate();

        if ($this->user_id) {
            $user = User::find($this->user_id);
            $old = User::find($this->user_id);

            $array = [
                'name' => $this->name,
                'mobile_number' => $this->mobile_number,
                'username' => $this->username,
                'password' => bcrypt($this->password),
            ];

            if (!$this->password) {
                $array = Arr::except($array, ['password']);
            }

            if (!$this->mobile_number) {
                $array = Arr::except($array, ['mobile_number']);
            }

            $user->update($array);
            $this->updateMode = false;
            activity()->withProperties(['old' => $old, 'new' => $user->refresh()])->log("Profile  Updated: $user->name");
            session()->flash('message', 'Profile Updated Successfully.');
            $this->resetInputFields();
            $this->emit('employeeUpdate'); // Close model to using to jquery

        }
    }

    public function cancel()
    {

    }

    public function render()
    {
        $user = User::findOrFail(auth()->user()->id);

        return view('livewire.profile.show-profile', compact('user'));
    }

    
}
