<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Arr;
use App\Models\User;


class ShowAdmins extends Component
{
    public $name;
    public $username;
    public $password;
    public $role;
    public $user_id;
    public $updateMode;

    protected $rules = [
        'name' => 'required|string|max:200',
        'username' => 'required|string|alpha_dash|unique:users',
        'password' => 'required|string|min:8',
        'role' => 'required|string|in:admin,super-admin'
    ];

    private function resetInputFields(){
        $this->name = '';
        $this->username = '';
        $this->password = '';
        $this->role = '';
    }

    public function store() 
    {
        $validatedData = $this->validate();

        $user = User::create([
            'name' => $this->name,
            'username' => $this->username,
            'password' => bcrypt($this->password)
        ]);

        $user->assignRole($this->role);

        session()->flash('message', 'Admin Created Successfully.');

        activity()->log("Admin Created: $user->name");

        $this->resetInputFields();

        $this->emit('employeeStore'); // Close model to using to jquery
    }

    public function edit($id) 
    {
        $this->updateMode = true;
        $user = User::where('id', $id)->first();
        $this->user_id = $id;

        $this->name = $user->name;
        $this->username = $user->username;
        $this->password = '';
        $this->role = $user->getRoleNames()->first();
    }

    public function update()
    {
        $this->rules['username'] = 'required|string|alpha_dash|unique:users,username,' .$this->user_id;
        $this->rules['password'] = 'nullable|string|min:8';
       
        $validatedData = $this->validate();

        if ($this->user_id) {
            $user = User::find($this->user_id);
            $old = User::find($this->user_id);

            $array = [
                'name' => $this->name,
                'username' => $this->username,
                'password' => bcrypt($this->password)
            ];

            if (!$this->password) {
                $array = Arr::except($array, ['password']);
            }

            $user->syncRoles([$this->role]);
            $user->update($array);
            
            $this->updateMode = false;
            activity()->withProperties(['old' => $old, 'new' => $user->refresh()])->log("Admin  Updated: $user->name");
            session()->flash('message', 'Admin Updated Successfully.');
            $this->resetInputFields();
            $this->emit('employeeUpdate'); // Close model to using to jquery

        }
    }

    public function cancel() 
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function delete($id)
    {

        if(auth()->user()->id === $id) 
        {
            $this->emit('adminDeleteWarning');
            return;
        }

        if($id){
            $user = User::findOrFail($id);
            activity()->withProperties(['deleted' => $user])->log("Admin Deleted: $user->name");
            $user->delete();
            session()->flash('message', 'Admin Deleted Successfully.');
        }
    }


    public function render()
    {
        $admins = User::role(['admin', 'super-admin'])->simplePaginate(10);
        
        return view('livewire.admin.show-admins', compact('admins'));
    }
}
