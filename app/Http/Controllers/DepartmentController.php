<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;

class DepartmentController extends Controller
{
    public function index()
    {
        return view('pages.department.index');
    }

    public function show($id)
    {
        $department = Department::findOrFail($id);
        return view('pages.department.show', compact('department'));
    }
}
