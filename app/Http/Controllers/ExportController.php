<?php

namespace App\Http\Controllers;
use App\Exports\TimesheetExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

use App\Models\User;

class ExportController extends Controller
{
    public function export(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $user_id = $request->has('user_id') ? $request->user_id : false;
        $department = $request->has('department') ? $request->department : false;

        return Excel::download(new TimesheetExport($from, $to, $department, $user_id), 'reports.xlsx');
    }
}
