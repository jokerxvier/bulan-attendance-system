<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('pages.employee.index');
    }

    public function show($id)
    {
        $user = User::role('employee')->whereId($id)->firstOrFail();

        return view('pages.employee.show', compact('user'));
    }
}
