<?php

namespace App\Http\Controllers;
use App\Charts\DepartmentChart;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use Spatie\Activitylog\Models\Activity;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {

        $latestActivities = Activity::latest()->limit(7)->get(); 

        $total_employee = User::role('employee')->count();
        $total_department = Department::count();
        $total_new = User::role('employee')->where('created_at','>=',Carbon::today()->subDays(30))->count();

        $department = Department::orderBy('id')->pluck('name');
        $user_group_by_department = User::select('id', 'department_id', \DB::raw("count(id)  as total_count"))
                ->role('employee')
                ->orderBy('department_id')
                ->groupBy('department_id')->pluck('total_count');

        $chart = new DepartmentChart;
        $chart->labels($department->toArray());
        $chart->dataset('Personnel Count', 'bar', $user_group_by_department->toArray())->options([
            'color' => '#4e73df',
            'fill' => '#4e73df',
            'backgroundColor' => '#4e73df'
        ]);

    
        return view('pages.dashboard', compact('total_employee', 'total_department', 'total_new', 'chart', 'latestActivities'));
    }
}
