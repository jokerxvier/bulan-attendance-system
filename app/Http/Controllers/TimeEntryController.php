<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TimeEntry;
use Carbon\Carbon;

class TimeEntryController extends Controller
{
    public function index()
    {
        return view('pages.time-entry.index');
    }


    public function timeIn(Request $request)
    {
        $user_id = $request->user_id;

        $user = User::findOrFail($user_id);

        $timeEntry = TimeEntry::whereHas('user', function ($query) use($user) {
            $query->where('id', $user->id);
        });

        $now = Carbon::now();

        $current_time = $now; //Carbon::create($now->year, $now->month, $now->day, 17, 00, 0)

        $morning_start = Carbon::create($now->year, $now->month, $now->day, 0, 0, 0);
        $morning_end = Carbon::create($now->year, $now->month, $now->day, 12, 30, 0);

        $morning_late_time = Carbon::create($now->year, $now->month, $now->day, 8, 0, 0);

        if ($current_time->between($morning_start, $morning_end)) {
            $timeEntry = $timeEntry->whereDate('time_start_am', '=', $current_time->format('Y-m-d'))->first();

            if($timeEntry) {
                $timeEntry->update([
                    'time_end_am' => $current_time->format('Y-m-d H:i')
                ]);

                return response()->json([
                    'time_in' => $timeEntry->time_start_am->format('g:i A'),
                    'time_out' => $current_time->format('g:i A'),
                    'name' => ucfirst($user->name)
                ]);
            }else {

                if($current_time->greaterThan($morning_late_time)) {
                    $user->late_count = $user->late_count + 1;
                    $user->save();
                }
    
                $user->timeEntries()->create([
                    'time_start_am' => $current_time->format('Y-m-d H:i')
                ]);

                return response()->json([
                    'time_in' => $current_time->format('g:i A'),
                    'time_out' => null,
                    'name' => ucfirst($user->name)
                ]);
            }

        }else {
            $timeEntry = $timeEntry->whereDate('created_at', '=', $current_time->format('Y-m-d'))->first();
    
            if($timeEntry) {
                if(!$timeEntry->time_start_pm && !$timeEntry->time_end_pm) {
                    $timeEntry->update([
                        'time_start_pm' => $current_time->format('Y-m-d H:i')
                    ]);

                    return response()->json([
                        'time_in' => $current_time->format('g:i A'),
                        'time_out' => null,
                        'name' => ucfirst($user->name)
                    ]);
                }else {

                    $timeEntry->update([
                        'time_end_pm' => $current_time->format('Y-m-d H:i')
                    ]);

                    return response()->json([
                        'time_in' => $timeEntry->time_start_pm->format('g:i A'),
                        'time_out' => $current_time->format('g:i A'),
                        'name' => ucfirst($user->name)
                    ]);

                }
                
            }else {
                $user->timeEntries()->create([
                    'time_start_pm' => $current_time->format('Y-m-d H:i')
                ]);

                return response()->json([
                    'time_in' => $current_time->format('g:i A'),
                    'time_out' => null,
                    'name' => ucfirst($user->name)
                ]);
                
            }
        }

    }

    public function export()
    {
        return view('pages.time-entry.export');
    }
}
