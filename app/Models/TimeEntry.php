<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TimeEntry extends Model
{
    use HasFactory;

    protected $dates = [
        'time_start_am',
        'time_end_am',
        'time_start_pm',
        'time_end_pm',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'time_start_am',
        'time_end_am',
        'time_start_pm',
        'time_end_pm',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function setTimeStartAmAttribute($value)
    {
        $this->attributes['time_start_am'] = $value ? Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s') : null;
    }

    public function setTimeEndAmAttribute($value)
    {
        $this->attributes['time_end_am'] = $value ? Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s') : null;
    }

    public function setTimeStartPmAttribute($value)
    {
        $this->attributes['time_start_pm'] = $value ? Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s') : null;
    }

    public function setTimeEndPmAttribute($value)
    {
        $this->attributes['time_end_pm'] = $value ? Carbon::createFromFormat('Y-m-d H:i', $value)->format('Y-m-d H:i:s') : null;
    }

    public function getTimeInAmAttribute()
    {
        return $this->time_start_am ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_start_am) : null;
    }

    public function getTimeOutAmAttribute()
    {
        return $this->time_end_am ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_end_am): null;
    }


    public function getTimeInPmAttribute()
    {
        return $this->time_start_pm ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_start_pm): null;
    }

    public function getTimeOutPmAttribute()
    {
        return $this->time_end_pm ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_end_pm) : null;
    }


    public function getTimeInStatusAttribute()
    {
        $morning = Carbon::create($this->time_start->year, $this->time_start->month, $this->time_start->day, 8, 0, 0);
        $afternoon = Carbon::create($this->time_start->year, $this->time_start->month, $this->time_start->day, 13, 0, 0);

        if($this->time_start->format('A') === 'PM') {
            return $this->time_start->lte($afternoon) ? true : false;
        }else {
            return $this->time_start->lte($morning) ? true : false;
        }
    }

    

    public function getTotalTimeAttribute()
    {
  

        if($this->time_start_am && $this->time_end_pm && !$this->time_end_am) {

            $time_start = $this->time_start_am ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_start_am) : 0;
            $time_end = $this->time_end_pm ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_end_pm) : 0;

            $total = $time_end ?  $time_end->diffInSeconds($time_start) : 0;
            
            $nightShiftStart = $this->time_start_am ? Carbon::create($time_start->year, $time_start->month, $time_start->day, 01, 0, 0) : 0;
            $nightShiftEnd = $this->time_start_am ? Carbon::create($time_start->year, $time_start->month, $time_start->day, 03, 59, 59) : 0;

            if ($time_start->between($nightShiftStart, $nightShiftEnd)) {
                return gmdate("H:i", $total - 28800);
            }else {
                return gmdate("H:i", $total - 3600);
            }

        }else {

            $time_start_am = $this->time_start_am ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_start_am) : 0;
            $time_end_am = $this->time_end_am ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_end_am) : 0;

            $time_start_pm = $this->time_start_pm ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_start_pm) : 0;
            $time_end_pm = $this->time_end_pm ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_end_pm) : 0;

            $secondsAm =  $time_end_am ?  $time_end_am->diffInSeconds($time_start_am) : 0;
            $secondsPm =  $time_end_pm ?  $time_end_pm->diffInSeconds($time_start_pm) : 0;

            return gmdate("H:i", $secondsAm + $secondsPm);

        }

        
    }

}
