<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Models\TimeEntry;
use App\Models\User;
use App\Models\Department;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

class TimesheetExport implements FromQuery, WithMapping, WithColumnWidths, WithHeadings, WithStyles
{
    use Exportable;

    protected $user_id;
    protected $from;
    protected $to;
    protected $department;

    public function __construct($from, $to, $department = false, $user_id = false) {
        $this->user_id = $user_id;
        $this->from = $from;
        $this->to = $to;
        $this->department = $department;
    }

    public function query()
    {
   
        $from = Carbon::parse($this->from)->format('Y-m-d');
        $to = Carbon::parse($this->to)->format('Y-m-d');
        $department = $this->department;

        $data = TimeEntry::query();

        if($this->user_id) {
            $data = $data->whereUserId($this->user_id);
        }

        if($department) {
            $data = $data->whereHas('user', function($q) use($department) {
                $q->where('department_id', '=', $department);
            });
        }
            
        $data = $data->whereBetween(DB::raw('date(created_at)'), [$from, $to])->orderBy('created_at', 'ASC');
        
        return $data;
    }

    public function map($data): array
    {
        if($this->user_id) {
            return [
                $data->created_at->format('F j, Y'),
                optional($data->created_at)->format('l'),
                optional($data->time_start_am)->format('g:i A'),
                optional($data->time_end_am)->format('g:i A'),
                '',
                optional($data->time_start_pm)->format('g:i A'),
                optional($data->time_end_pm)->format('g:i A'),
                optional($data)->total_time
                // $invoice->user->name,
                // Date::dateTimeToExcel($invoice->created_at),
            ];
        }else {
            return [
                $data->user->name,
                $data->created_at->format('F j, Y'),
                optional($data->created_at)->format('l'),
                optional($data->time_start_am)->format('g:i A'),
                optional($data->time_end_am)->format('g:i A'),
                '',
                optional($data->time_start_pm)->format('g:i A'),
                optional($data->time_end_pm)->format('g:i A'),
                optional($data)->total_time
                // $invoice->user->name,
                // Date::dateTimeToExcel($invoice->created_at),
            ];
        }
        
    }


    public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 20,   
            'C' => 20,
            'D' => 20,
            'E' => 20,
            'F' => 5,
            'G' => 20,
            'H' => 20,
            'I' => 20    
        ];
    }

    public function headings(): array
    {
        $PersonnelName = [];
        $Department = [];

        $StartDate = ['Start Date:', Carbon::parse($this->from)->format('F j, Y')];
        $EndDate = ['End Date:', Carbon::parse($this->to)->format('F j, Y')];

        if($this->user_id) {
            $user = User::find($this->user_id);

            if($user) {
                $PersonnelName = ['Personnel Name:', ucfirst($user->name)];
                $Department = ['Department:', $user->departments->name];
            }
        }

        if($this->department) {
            $dept = Department::find($this->department);

            if($dept) {
                $Department = ['Department:', $dept->name];
            }
        }

        if($this->user_id)
        {
            return [
                $PersonnelName,
                $Department,
                $StartDate,
                $EndDate,
                [],
                ['Date','Day','Time IN','Time Out','','Time IN','Time Out','Total'],
                
            ];
        }else if ($this->department) {
            return [
                $Department,
                $StartDate,
                $EndDate,
                [],
                ['Name','Date','Day','Time IN','Time Out','','Time IN','Time Out','Total'],
                
            ];
        }else {
            return [
                'Date','Day','Time IN','Time Out','','Time IN','Time Out','Total',
            ];
        }       
    }

    public function styles(Worksheet $sheet)
    {
        if ($this->user_id) {
            return [
                // Style the first row as bold text.
                'A1'    => ['font' => ['bold' => true]],
                'A2'    => ['font' => ['bold' => true]],
                'A3'    => ['font' => ['bold' => true]],
                'A4'    => ['font' => ['bold' => true]],
                '6'    => ['font' => ['bold' => true]],
            ];
        }else if($this->department) {
            return [
                'A1'    => ['font' => ['bold' => true]],
                'A2'    => ['font' => ['bold' => true]],
                'A3'    => ['font' => ['bold' => true]],
                '5'    => ['font' => ['bold' => true]],
            ];
        }else {
            return [
                '1'    => ['font' => ['bold' => true]],
            ];
        }
        
    }
}
