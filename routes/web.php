<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\TimeEntryController;
use App\Http\Controllers\QrcodeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ProfileController;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::prefix('login')->group(function () {
    Route::get('/', [LoginController::class, 'index'])->name('login');
});

Route::post('/logout', [LoginController::class, 'logout'])->name('logout');


Route::middleware(['auth'])->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        

        Route::prefix('departments')->group(function () {
            Route::get('/', [DepartmentController::class, 'index'])->name('department');
            Route::get('/{id}', [DepartmentController::class, 'show'])->name('department.show');
        });

        Route::prefix('employees')->group(function () {
            Route::get('/', [EmployeeController::class, 'index'])->name('employee');
            Route::get('/{id}', [EmployeeController::class, 'show'])->name('employee.show');
        });

        Route::prefix('time-entries')->group(function () {
            Route::get('/', [TimeEntryController::class, 'index'])->name('time.entries');
            Route::get('/export', [TimeEntryController::class, 'export'])->name('time.export');
        });

        Route::prefix('export')->group(function () {
            Route::get('/', [ExportController::class, 'export'])->name('export');
        });
    });

    Route::group(['middleware' => ['role:super-admin']], function () {
        Route::prefix('admins')->group(function () {
            Route::get('/', [AdminController::class, 'index'])->name('admin');
        });
    });
    
    
    
});

Route::prefix('attendance')->group(function () {
    Route::get('/time-in', [TimeEntryController::class, 'timeIn'])->name('attendance.timein');
    Route::post('/time-in', [TimeEntryController::class, 'timeIn'])->name('attendance.timein');
});



Route::prefix('qr-code')->group(function () {
    Route::get('/scan', [QrcodeController::class, 'index']);
    Route::get('/download/{user_id}', [QrcodeController::class, 'download']);
});
